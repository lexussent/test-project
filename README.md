Presented is a two stage CI/CD pipeline using Gitlab, with the aim of testing and packaging a nodejs file. The latest node docker image was chosen as it has all of the commands needed in the container upon creation.

The first stage runs a test, which echos "hello world" if found in the package.json file. The second stage packages the application into an artifact, which is stored as a .tgz file. I have tested that the artifact works by downloading it, running "npm start" and then browsing to localhost:3000.
